package com.ibmtest.cards.application.resources;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ibmtest.cards.application.services.AdviserService;
import com.ibmtest.cards.domain.Adviser;
import com.ibmtest.cards.infrastructure.AdviserRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(AdviserResource.class)
public class AdviserResourceTest {
	
	@Autowired private MockMvc mockMvc;
    
    @Autowired
    AdviserResource resource;
    @MockBean
    AdviserService service;
    @MockBean
    AdviserRepository repository;
    
    private List<Adviser> clientes;
    @Before("")
    public void setup() throws Exception {
//        mockMvc = standaloneSetup(this.resource).build();
        
        Adviser client1 = new Adviser();
        Adviser client2 = new Adviser();
        clientes = new ArrayList<>();
        clientes.add(client1);
        clientes.add(client2);
    }
    
    @Test
    public void testSearchSync() throws Exception {
        when(repository.findAll()).thenReturn(clientes);
        
        mockMvc.perform(get("/advisers").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}