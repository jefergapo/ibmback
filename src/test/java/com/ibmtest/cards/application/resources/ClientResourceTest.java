package com.ibmtest.cards.application.resources;


import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.ibmtest.cards.application.services.ClientService;
import com.ibmtest.cards.domain.Client;
import com.ibmtest.cards.infrastructure.ClientRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientResource.class)
public class ClientResourceTest {

	
	@Autowired
	private MockMvc mockMvc;
    
    @MockBean
    ClientService service;
    @MockBean
    ClientRepository repository;
    
    private List<Client> clients;
    
    @Before("")
    public void setup() throws Exception {
        
        
    }
    
    @Test
    public void testGetClientsWithData() throws Exception {
    	
    	Client client1 = new Client("1", "gtbtf", "gbbgf", "fsdf", "fsdfsd");
        Client client2 = new Client("2", "fafas", "sadfsdf", "sdfasdf", "sadfaf");
        
        when(repository.findAll()).thenReturn(Arrays.asList(client1, client2));
        
        mockMvc.perform(get("/clients").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].id", is("1")));
    }
    
    @Test
    public void testGetClientsWithNoData() throws Exception {
        
        when(repository.findAll()).thenReturn(Arrays.asList());
        
        mockMvc.perform(get("/clients").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(0)));
    }
    
}