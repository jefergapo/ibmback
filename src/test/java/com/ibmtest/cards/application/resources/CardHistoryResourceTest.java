package com.ibmtest.cards.application.resources;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ibmtest.cards.application.services.CardHistoryService;
import com.ibmtest.cards.domain.CardHistory;
import com.ibmtest.cards.infrastructure.CardHistoryRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(CardHistoryResource.class)
public class CardHistoryResourceTest {
	
	@Autowired private MockMvc mockMvc;
    
    @Autowired
    CardHistoryResource resource;
    @MockBean
    CardHistoryService service;
    @MockBean
    CardHistoryRepository repository;
    
    private List<CardHistory> clientes;
    @Before("")
    public void setup() throws Exception {
//        mockMvc = standaloneSetup(this.resource).build();
        
        CardHistory client1 = new CardHistory();
        CardHistory client2 = new CardHistory();
        clientes = new ArrayList<>();
        clientes.add(client1);
        clientes.add(client2);
    }
    
    @Test
    public void testSearchSync() throws Exception {
        when(repository.findAll()).thenReturn(clientes);
        
        mockMvc.perform(get("/card-transactions").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}