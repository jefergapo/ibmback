package com.ibmtest.cards.application.resources;

import static org.hamcrest.CoreMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ibmtest.cards.application.services.CardService;
import com.ibmtest.cards.domain.Card;
import com.ibmtest.cards.infrastructure.CardRepository;


@RunWith(SpringRunner.class)
@WebMvcTest(CardResource.class)
public class CardResourceTest {
	
	@Autowired private MockMvc mockMvc;
    
    @Autowired
    CardResource resource;
    @MockBean
    CardService service;
    @MockBean
    CardRepository repository;
    
    private List<Card> cards;
    @Before("")
    public void setup() throws Exception {
//        mockMvc = standaloneSetup(this.resource).build();
        
        Card card1 = new Card();
        Card card2 = new Card();
        cards = new ArrayList<>();
        cards.add(card1);
        cards.add(card2);
    }
    
    @Test
    public void testSearchSync() throws Exception {
        when(repository.findByClientId("") ).thenReturn(cards);
        
        mockMvc.perform(get("/cards/123-456-789").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}