INSERT INTO CLIENT (ID, ADDRES, CITY, NAME, PHONE_NUMBER) VALUES 
('0f6e599d-90c3-4268-b82d-37af4eac24de', 'Cra', 'Medellin', 'Jefferson', '3201212'),
('0f6e599d-90c3-4268-b82d-37af4eac24ed', 'Av', 'USA', 'Gerge Soros', '3201212'),
('0f7e599d-90c3-4268-b82d-38af4eac24ed', 'Cll', 'USA', 'Jack Ma', '4567894'),
('0f7e599d-90c3-4268-b82d-39af4eac24ed', 'Avn', 'USA', 'Roger Federer', '77799944'),
('0f7e599d-90c3-4268-b82d-31af4eac24ed', 'Cll', 'USA', 'Nicola Tesla', '116644');

INSERT INTO CARD (ID, CCV, CLIENT_ID, NUMBER, TYPE) VALUES 
('ed792884-9552-4a0b-94ae-6e20ef94fb16', 123, '0f6e599d-90c3-4268-b82d-37af4eac24de', '3216549870123456', 'Premium'),
('ed792885-9552-4a0b-94ae-6e20ef94fb16', 123, '0f6e599d-90c3-4268-b82d-37af4eac24de', '3216549870123456', 'Premium'),
('ed792886-9552-4a0b-94ae-6e20ef94fb16', 123, '0f6e599d-90c3-4268-b82d-37af4eac24de', '3216549870123456', 'Premium'),
('ed792882-9552-4a0b-94ae-6e20ef94fb16', 123, '0f6e599d-90c3-4268-b82d-37af4eac24ed', '3216549870123456', 'Premium'),
('ed792891-9552-4a0b-94ae-6e20ef94fb16', 123, '0f7e599d-90c3-4268-b82d-38af4eac24ed', '3216549870123456', 'Premium'),
('ed792892-9552-4a0b-94ae-6e20ef94fb16', 123, '0f7e599d-90c3-4268-b82d-38af4eac24ed', '3216549870123456', 'Premium'),
('ed792893-9552-4a0b-94ae-6e20ef94fb16', 123, '0f7e599d-90c3-4268-b82d-38af4eac24ed', '3216549870123456', 'Premium');

INSERT INTO CARD_HISTORY (ID, AMOUNT, CARD_ID, DESCRIPTION, TRANSACTION_DATE) VALUES 
('24cd1c0e-611d-45f2-9f33-ec218b3e06ea', 3000000, 'ed792884-9552-4a0b-94ae-6e20ef94fb16', 'Compras', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss')),
('25cd1c0e-611d-45f2-9f33-ec218b3e06ea', 3000000, 'ed792882-9552-4a0b-94ae-6e20ef94fb16', 'Compras', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss')),
('26cd1c0e-611d-45f2-9f33-ec218b3e06ea', 3000000, 'ed792882-9552-4a0b-94ae-6e20ef94fb16', 'Compras', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss')),
('27cd1c0e-611d-45f2-9f33-ec218b3e06ea', 3000000, 'ed792882-9552-4a0b-94ae-6e20ef94fb16', 'Compras', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'));	

																											 
																											 
																											 
INSERT INTO USER_AUTH (ID, PASSWORD, USERNAME) VALUES 
('fef5e7b8-fc87-4b33-bcfb-6cfa7fddecd9', 'jgaleano', 'jgaleano'),
('fef5e7b9-fc87-4b33-bcfb-6cfa7fddecd9', 'admin', 'admin');

INSERT INTO ADVISER (ID, NAME, SPECIALTY, USER_ID) VALUES 
('48e0a0c1-52e1-43d6-ad3e-6fc02b17eb82', 'Jefferson', 'Desarrollo de Software', 'fef5e7b8-fc87-4b33-bcfb-6cfa7fddecd9'),
('52e0a0c1-52e1-43d6-ad3e-6fc02b17eb82', 'Steven Gerard', 'Futbolista', 'fef5e7b9-fc87-4b33-bcfb-6cfa7fddecd9');
