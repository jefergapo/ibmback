package com.ibmtest.cards.infrastructure;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ibmtest.cards.domain.UserAuth;

public interface UserAuthRepository extends JpaRepository<UserAuth, String> {
	
	Optional<UserAuth> findByUsername(String username);

}