package com.ibmtest.cards.infrastructure;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ibmtest.cards.domain.Adviser;

public interface AdviserRepository extends JpaRepository<Adviser, String> {
	
	Optional<Adviser> findByUserUsername(String username);

}
