package com.ibmtest.cards.infrastructure;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ibmtest.cards.domain.Client;

public interface ClientRepository extends JpaRepository<Client, String> {
	
	List<Client> findAll();
	
	void deleteById(String id);
	
	<S extends Client> S save(S entity);

}
