package com.ibmtest.cards.infrastructure;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ibmtest.cards.domain.Card;

public interface CardRepository extends JpaRepository<Card, String>  {
		
	List<Card> findByClientId(String id);

}
