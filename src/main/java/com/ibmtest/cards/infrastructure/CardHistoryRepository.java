package com.ibmtest.cards.infrastructure;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ibmtest.cards.domain.CardHistory;

public interface CardHistoryRepository extends JpaRepository<CardHistory, String> {
	
	List<CardHistory> findByCardId(String id);
	
}
