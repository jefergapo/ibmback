package com.ibmtest.cards.application.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ibmtest.cards.application.services.AdviserService;
import com.ibmtest.cards.domain.Adviser;
import com.ibmtest.cards.infrastructure.AdviserRepository;

@RestController
public class AdviserResource {
	
	private AdviserRepository adviserRepo;
	private AdviserService adviserService;

	public AdviserResource(AdviserRepository repo, AdviserService adviserService) {
		this.adviserRepo = repo;
		this.adviserService = adviserService;
	}
	
    @GetMapping("/advisers")
    List<Adviser> getAdvisers() {
        return adviserRepo.findAll();
    }
    
    @GetMapping("/loging")
    ResponseEntity<?> isLoggedIn(Authentication authentication) {
    	if (authentication.isAuthenticated()) return ResponseEntity.accepted().build();
    	return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
    
    @PostMapping("/advisers")
    Adviser saveAdviser(@Valid @RequestBody Adviser adviser) {
    	return adviserService.save(adviser);
    }
    
    @DeleteMapping("/advisers/{id}")
    ResponseEntity<?> deleteAdviser(@PathVariable("id") String id) {
    	adviserService.deleteById(id);
    	return ResponseEntity.ok().build();
    }

}