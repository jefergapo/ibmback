package com.ibmtest.cards.application.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ibmtest.cards.application.services.ClientService;
import com.ibmtest.cards.domain.Client;
import com.ibmtest.cards.infrastructure.ClientRepository;

@RestController
public class ClientResource extends ResourceBase {
	
	private ClientRepository clientRepo;
	private ClientService clientService;
	
	public ClientResource(ClientRepository repo, ClientService clientService) {
		this.clientRepo = repo;
		this.clientService = clientService;
	}
	
    @GetMapping("/clients")
    List<Client> getClients() {
        return clientRepo.findAll();
    }
    
    @GetMapping("/clients/{id}")
    ResponseEntity<Client> getClientById(@PathVariable("id") String id) {
        return resolveOptional(clientRepo.findById(id));
    }
    
    @PostMapping("/clients")
    Client saveClient(@Valid @RequestBody Client client) {
    	return clientService.save(client);
    }
    
    @DeleteMapping("/clients/{id}")
    ResponseEntity<?> deleteClient(@PathVariable("id") String id) {
    	clientService.deleteById(id);
    	return ResponseEntity.ok().build();
    }

}
