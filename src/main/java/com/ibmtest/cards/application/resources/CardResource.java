package com.ibmtest.cards.application.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ibmtest.cards.application.services.CardService;
import com.ibmtest.cards.domain.Card;
import com.ibmtest.cards.infrastructure.CardRepository;

@RestController
public class CardResource extends ResourceBase {
	
	private CardRepository cardRepo;
	private CardService cardService;
	
	public CardResource(CardRepository cardRepo, CardService cardService) {
		this.cardRepo = cardRepo;
		this.cardService = cardService;
	}
	
    @GetMapping("/cards/{id}")
    ResponseEntity<Card> getCard(@PathVariable("id") String id) {
        return resolveOptional(cardRepo.findById(id));
    }
    
    @GetMapping("/clients/{id}/cards")
    List<Card> getCardsByClientId(@PathVariable("id") String id) {
        return cardRepo.findByClientId(id);
    }
    
    @PostMapping("/cards")
    Card saveCard(@Valid @RequestBody Card card) {
    	return cardService.save(card);
    }
    
    @DeleteMapping("/cards/{id}")
    ResponseEntity<?> deleteCard(@PathVariable("id") String id) {
    	cardService.deleteById(id);
    	return ResponseEntity.ok().build();
    }

}
