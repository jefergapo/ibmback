package com.ibmtest.cards.application.resources;

import java.util.Optional;

import org.springframework.http.ResponseEntity;

public abstract class ResourceBase {
	
	protected <T> ResponseEntity<T> resolveOptional(Optional<T> option) {
  	  return option
  			  .map(o ->  ResponseEntity.ok(o) )
  			  .orElse(ResponseEntity.noContent().build());
	}
}
