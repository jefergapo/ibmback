package com.ibmtest.cards.application.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ibmtest.cards.application.services.CardHistoryService;
import com.ibmtest.cards.domain.CardHistory;
import com.ibmtest.cards.infrastructure.CardHistoryRepository;

@RestController
public class CardHistoryResource extends ResourceBase {
	
	private CardHistoryRepository cardHistoryRepo;
	private CardHistoryService cardHistoryService;
	
	public CardHistoryResource(CardHistoryRepository repo, CardHistoryService cardHistoryRService) {
		this.cardHistoryRepo = repo;
		this.cardHistoryService = cardHistoryRService;
	}
	
	
    @GetMapping("/card-transactions/{id}")
    ResponseEntity<CardHistory> getCardHistorys(@PathVariable("id") String id) {
        return resolveOptional(cardHistoryRepo.findById(id));
    }
    
    @GetMapping("/cards/{id}/card-transactions")
    List<CardHistory> getCardHistoryByCardId(@PathVariable("id") String id) {
        return cardHistoryRepo.findByCardId(id);
    }
    
    @PostMapping("/card-transactions")
    CardHistory saveCardHistory(@Valid @RequestBody CardHistory cardHistory, Authentication authentication) {
    	return cardHistoryService.save(cardHistory, authentication);
    }
    
    @DeleteMapping("/card-transactions/{id}")
    ResponseEntity<?> deleteCardHistory(@PathVariable("id") String id) {
    	cardHistoryService.deleteById(id);
    	return ResponseEntity.ok().build();
    }

}
