package com.ibmtest.cards.application.services;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.ibmtest.cards.domain.Client;
import com.ibmtest.cards.infrastructure.ClientRepository;

@Service
public class ClientService {

    private final ClientRepository clientRepo;

    ClientService(ClientRepository repo) {
        this.clientRepo = repo;
    }
    
    @Transactional()
    public Client save(Client client) {
    	return clientRepo.save(client);
    }
    
    public void deleteById(String id) {
    	clientRepo.deleteById(id);
    }
}
