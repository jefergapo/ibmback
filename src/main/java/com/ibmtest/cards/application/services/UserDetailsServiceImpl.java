package com.ibmtest.cards.application.services;

import static java.util.Collections.emptyList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ibmtest.cards.infrastructure.UserAuthRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	UserAuthRepository userRepo;

	public UserDetailsServiceImpl(UserAuthRepository userRepo) {
		this.userRepo = userRepo;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepo.findByUsername(username)
				.map( user -> new User(user.getUsername(), user.getPassword(), emptyList()))
				.orElseThrow( () -> new UsernameNotFoundException(username));
	}

}
