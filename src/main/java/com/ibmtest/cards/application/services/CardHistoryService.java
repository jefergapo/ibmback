package com.ibmtest.cards.application.services;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.ibmtest.cards.domain.CardHistory;
import com.ibmtest.cards.infrastructure.CardHistoryRepository;


@Service
public class CardHistoryService {
	
	private static final Logger logger = LoggerFactory.getLogger(CardHistoryService.class);

    private final CardHistoryRepository cardHistoryRepo;

    CardHistoryService(CardHistoryRepository repo) {
        this.cardHistoryRepo = repo;
    }
    
    @Transactional()
    public CardHistory save(CardHistory card, Authentication authentication) {
    	
    	logger.debug(card.toString());
    	logger.info(card.toString());
    	return cardHistoryRepo.save(card);
    }
    
    public void deleteById(String id) {
    	cardHistoryRepo.deleteById(id);
    }
}