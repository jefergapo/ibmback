package com.ibmtest.cards.application.services;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.ibmtest.cards.domain.Card;
import com.ibmtest.cards.infrastructure.CardRepository;

@Service
public class CardService {

    private final CardRepository cardRepo;

    CardService(CardRepository repo) {
        this.cardRepo = repo;
    }
    
    @Transactional()
    public Card save(Card card) {
    	return cardRepo.save(card);
    }
    
    public void deleteById(String id) {
    	cardRepo.deleteById(id);
    }
}