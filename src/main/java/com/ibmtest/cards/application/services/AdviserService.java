package com.ibmtest.cards.application.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.ibmtest.cards.domain.Adviser;
import com.ibmtest.cards.domain.UserAuth;
import com.ibmtest.cards.infrastructure.AdviserRepository;
import com.ibmtest.cards.infrastructure.UserAuthRepository;

@Service
public class AdviserService {

    private final AdviserRepository adviserRepo;
    private final UserAuthRepository userRepo;

    AdviserService(AdviserRepository adviserRepo, UserAuthRepository userRepo) {
        this.adviserRepo = adviserRepo;
        this.userRepo = userRepo;
    }
    
    @Transactional()
    public Adviser save(Adviser adviser) {
    	UserAuth user = adviser.getUser();
    	Optional<UserAuth> userFinded = userRepo.findByUsername(user.getUsername());
    	if (userFinded.isPresent()) throw new RuntimeException("Ya existe el usuario");
    	UserAuth savedUser = userRepo.save(user);
    	adviser.setUser(savedUser);
    	return adviserRepo.save(adviser);
    }
    
    public void deleteById(String id) {
    	adviserRepo.deleteById(id);
    }
}